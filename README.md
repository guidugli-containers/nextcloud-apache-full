# nextcloud-apache-full

Provide Nextcloud image based on apache with cron and smbclient added.

## Usage

Clone the repository and use the create-pod.sh and destroy-pod to create or destroy Nextcloud pod container Nextcloud, Redis and MariaDB.

## License
BSD
