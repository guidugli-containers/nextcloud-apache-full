#!/bin/bash

DATA_PATH=/opt/Nextcloud

podman pod create --name Nextcloud --publish 9090:80

mkdir -p $DATA_PATH/nextcloud-app $DATA_PATH/nextcloud-data $DATA_PATH/mariadb

# Deploy Mariadb
podman run --detach --pod Nextcloud \
  --env MYSQL_DATABASE=nextcloud \
  --env MYSQL_USER=nextcloud \
  --env MYSQL_PASSWORD=MyIn1t1@lP@ssw0rd \
  --env MYSQL_ROOT_PASSWORD=R00tIn1t1@alP@ssw0rd \
  --volume $DATA_PATH/mariadb:/var/lib/mysql \
  --restart on-failure \
  --name nextcloud-db \
  docker.io/library/mariadb:10.5

# Deploy Redis
podman run --detach --pod Nextcloud \
  --restart on-failure \
  --name nextcloud-redis \
  redis:alpine

# Deploy Nextcloud
podman run --detach --pod Nextcloud \
  --env MYSQL_HOST=nextcloud \
  --env MYSQL_DATABASE=nextcloud \
  --env MYSQL_USER=nextcloud \
  --env MYSQL_PASSWORD=MyIn1t1@lP@ssw0rd \
  --env REDIS_HOST=nextcloud \
  --env PHP_MEMORY_LIMIT=512M \
  --env NEXTCLOUD_TRUSTED_DOMAINS="nc.local.exemple.net 127.0.0.1" \
  --env OVERWRITEPROTOCOL=https \
  --env TRUSTED_PROXIES="192.168.100.0/24 10.0.0.150 127.0.0.1" \
  --env APACHE_DISABLE_REWRITE_IP=1 \
  --env NEXTCLOUD_HOSTNAME=nc.local.exemple.net \
  --volume $DATA_PATH/nextcloud-app:/var/www/html \
  --volume $DATA_PATH/nextcloud-data:/var/www/html/data \
  --restart on-failure \
  --name nextcloud \
  docker.io/guidugli/nextcloud-apache-full:latest

